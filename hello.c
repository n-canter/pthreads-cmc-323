






#include <stdio.h>
#include <pthread.h>

#define NUM_THREADS 4

void *greeting(void *id)
{
	long thread_id = (long)id;
	printf("I'm %ld. Hello, World\n", thread_id);
	pthread_exit((void *)thread_id);
}

int main() {
	pthread_t threads[NUM_THREADS];
	
	for(long id = 0; id < NUM_THREADS; id++){
		pthread_create(threads+id, NULL, greeting, (void *)id);
	}
	void *status;
	for(int i = 0; i < NUM_THREADS; i++) {
		pthread_join(threads[i], &status);
		printf("joined with status: %ld\n", (long) status);
	}
}
